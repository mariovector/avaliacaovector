### PROBLEMA ###
Dado o seguinte log de um jogo de tiro em primeira pessoa:

```
#!
23/04/2015 15:34:22 - New match 11348965 has started
23/04/2015 15:36:04 - Roman killed Nick using M16
23/04/2015 15:36:33 - <WORLD> killed Nick by DROWN
23/04/2015 15:39:22 - Match 11348965 has ended

```


Escreva um código para:

* Montar o ranking de cada partida, com a quantidade assassinatos e a quantidade de mortes de cada jogador;

Observações:

* Assassinatos realizados pelo player <WORLD> devem ser desconsiderados.


### BÓNUS ###
(Não obrigatório. Faça apenas caso se identifique com o problema ou se achar que há algo interessante a ser mostrado na solução)

* Descobrir a arma preferida (a que mais matou) do vencedor;
* Identificar a maior sequência de assassinatos efetuadas por um jogador (streak) sem morrer, dentro da partida;
* Jogadores que vencerem uma partida sem morrerem devem ganhar um "award";
* Jogadores que matarem 5 vezes em 1 minuto devem ganhar um "award".


### SOLUÇÃO ###
* Seja criativo;
* Explore ao máximo a orientação a objetos;
* Crie testes unitários e tente uar TDD;
* não é necessário utilizar nenhum framework.
* Só efetue o Pull Request após a solução estar finalizada.
* Você pode editar esse arquivo para deixar qualquer comentários / observações.

### BOA SORTE! ###